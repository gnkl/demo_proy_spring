<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="addContactsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="addContactsModalLabel" class="displayInLine">
                    <spring:message code="create"></spring:message>&nbsp;<spring:message code="contact"></spring:message>
                    </h3>
                </div>        
                <div class="modal-body">
                    <form name="newContactForm" novalidate="">
                        <div>
                            <label>* <spring:message code="contacts.name"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="contact.name" name="name" placeholder="<spring:message code='contact'></spring:message>&nbsp;<spring:message code='contacts.name'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newContactForm.phoneNumber.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>
                        </div>
                        <div>
                            <label>* <spring:message code="contacts.email"></spring:message>:</label>
                            <input class="form-control" required="" ng-model="contact.email" name="email" placeholder="<spring:message code='sample.email'></spring:message> " type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newContactForm.phoneNumber.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>
                        </div>
                        <div>
                            <label>* <spring:message code="contacts.phone"></spring:message>:</label>
                            <input class="form-control" required="" ng-model="contact.phoneNumber" name="phoneNumber" placeholder="<spring:message code='sample.phone'></spring:message> " type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newContactForm.phoneNumber.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>
                        </div>            
                    </form>
                </div>
                <div class="modal-footer">
                    <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                    <input class="btn btn-primary" ng-click="createContact(newContactForm);" value="<spring:message code="create"></spring:message>" type="submit">
                    <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#addContactsModal');" aria-hidden="true">
                        <spring:message code="cancel"></spring:message>
                    </button>
            </div>
        </div>
    </div>
</div>

<div id="updateContactsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h3 id="updateContactsModalLabel" class="displayInLine">
                        <spring:message code="update"/>&nbsp;<spring:message code="contact"/>
                    </h3>
                </div>                      
                <div class="modal-body">
                    <form name="updateContactForm" novalidate>
                        <input type="hidden" required ng-model="contact.id" name="id" value="{{contact.id}}"/>

                        <div>
                            <label>* <spring:message code="contacts.name"/>:</label>
                            <input class="form-control" type="text" autofocus required ng-model="contact.name" name="name" placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='contacts.name'/> "/>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && updateContactForm.name.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>                            
                        </div>
                        <div>
                            <label>* <spring:message code="contacts.email"/>:</label>
                            <input class="form-control" type="text" required ng-model="contact.email" name="email" placeholder="<spring:message code='sample.email'/> "/>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && updateContactForm.email.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>
                        </div>
                        <div>
                            <label>* <spring:message code="contacts.phone"/>:</label>
                            <input class="form-control" type="text" required ng-model="contact.phoneNumber" name="phoneNumber" placeholder="<spring:message code='sample.phone'/> "/>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && updateContactForm.phoneNumber.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>                            
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                    <input type="submit" class="btn btn-primary" ng-click="updateContact(updateContactForm);" value='<spring:message code="update"/>'/>
                    <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#updateContactsModal');" aria-hidden="true"><spring:message code="cancel"/></button>
                </div>
        </div>
    </div>
</div>

                
<div id="deleteContactsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deleteContactsModalLabel" class="displayInLine">
                    <spring:message code="delete"/>&nbsp;<spring:message code="contact"/>
                </h3>
            </div>                
            <div class="modal-body">
                <form name="deleteContactForm" novalidate="">
                    <div>
                        <p><spring:message code="delete.confirm"/>:&nbsp;{{contact.name}}?</p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                <input type="submit" class="btn btn-primary" ng-click="deleteContact();" value='<spring:message code="delete"/>'/>
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#deleteContactsModal');" aria-hidden="true">
                    <spring:message code="cancel"/>
                </button>                 
            </div>
        </div>
    </div>
</div>          


<div id="searchContactsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h3 id="searchContactsModalLabel" class="displayInLine">
                        <spring:message code="search"/>
                    </h3>
                </div>                      
                <div class="modal-body">
                    <form name="searchContactForm" novalidate>
                        <label><spring:message code="search.for"/></label>
                        <div>
                            <input type="text" class="form-control" autofocus required ng-model="searchFor" name="searchFor" placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='contacts.name'/> "/>                            
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && searchContactForm.searchFor.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>                                   
                        </div>
                    </form>                    
                </div>
                <div class="modal-footer">
                    <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                

                    <input type="submit" class="btn btn-primary" ng-click="searchContact(searchContactForm, false);" value='<spring:message code="search"/>'/>
                    <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#searchContactsModal');" aria-hidden="true">
                        <spring:message code="cancel"/>
                    </button>                    
                </div>
        </div>
    </div>
</div>