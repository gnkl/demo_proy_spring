<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!--<div class="container">
    <h2 class="form-heading">Acceso denegado</h2>
    <h4 class="text-center"><a href="${contextPath}/login">Ir a login</a></h4>
</div>-->

<div class="alert alert-danger alert-dismissible fade in" role="alert">
    <h4>Tu sesión ha finalizado!</h4>
    <p>Para ir a pantalla de login da click en el siguiente boton</p>
    <p>
        <button id="myButton" class="btn btn-danger" type="button">Click para continuar</button>
    </p>
</div>

<script>
  $('#myButton').on('click', function () {
      window.location = "/trackingNovartis/login";
  })
</script>