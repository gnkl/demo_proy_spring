//    var app = angular.module('app', ['ngTouch', 'ui.grid', 'ui.grid.pagination']);
//     
//    app.controller('MainCtrl', [
//    '$scope', '$http', 'uiGridConstants', function($scope, $http, uiGridConstants) {
//     
//      $scope.url = "/trackingNovartis/protected/contacts/";
//     
//      var paginationOptions = {
//        pageNumber: 0,
//        pageSize: 25,
//        sort: null
//      };
//     
//      $scope.gridOptions = {
//        paginationPageSizes: [25, 50, 75],
//        paginationPageSize: 25,
//        useExternalPagination: true,
//        useExternalSorting: true,
//        columnDefs: [
//          { name: 'name' },
//          { name: 'gender', enableSorting: false },
//          { name: 'company', enableSorting: false }
//        ],
//        onRegisterApi: function(gridApi) {
//          $scope.gridApi = gridApi;
//          $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
//            if (sortColumns.length == 0) {
//              paginationOptions.sort = null;
//            } else {
//              paginationOptions.sort = sortColumns[0].sort.direction;
//            }
//            getPage();
//          });
//          gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
//            paginationOptions.pageNumber = newPage;
//            paginationOptions.pageSize = pageSize;
//            getPage();
//          });
//        }
//      };
//     
//      var getPage = function() {
//        var url;
//        url = $scope.url;
//        console.log(url);
//        var config = {params: {page: paginationOptions.pageNumber}};
//     
//        $http.get(url, config)
//        .success(function (data) {
//           console.log(data); 
//          $scope.gridOptions.totalItems = data.totalContacts;
//          //var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
//          //$scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);
//          $scope.gridOptions.data = data.contacts;
//        })        
//        .error(function (data) {
//           console.log(data); 
//           $scope.state = 'error';
//                //$scope.displayCreateContactButton = false;   
//          //$scope.gridOptions.totalItems = 100;
//          //var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
//          //$scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);
//          //$scope.gridOptions.data = data;
//        });
//      };
//     
//      getPage();
//    }
//    ]);

var app = angular.module('main', ['ngTable']);

app.config(function ($httpProvider) {
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
}); 

//app.controller('MainCtrl', ['$scope', '$http', function($scope, $http, ngTableParams) {
app.controller('MainCtrl', ['$scope', '$http', 'ngTableParams', '$location','$q',function($scope, $http, ngTableParams, $location,$q) {

    $scope.errorOnSubmit = false;
    $scope.errorIllegalAccess = false;
    $scope.displayMessageToUser = false;
    $scope.displayValidationError = false;
    $scope.displaySearchMessage = false;
    $scope.displaySearchButton = false;
    $scope.displayCreateContactButton = false;
    $scope.url = "/trackingNovartis/protected/contacts/";

    $scope.resetContact = function(){
        $scope.contact = {};
    };
    
    $scope.deleteContact = function () {
        $scope.lastAction = 'delete';

        var url = $scope.url + $scope.contact.id;

        $scope.startDialogAjaxRequest();

        var params = {searchFor: $scope.searchFor, page: $scope.pageToGet};

        $http({
            method: 'DELETE',
            url: url,
            params: params
        }).success(function (data) {
                $scope.resetContact();
                $scope.finishAjaxCallOnSuccess(data, "#deleteContactsModal", false);
            }).error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
            });
    };    

    $scope.updateContact = function (updateContactForm) {
        if (!updateContactForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }

        $scope.lastAction = 'update';

        var url = $scope.url + $scope.contact.id;

        $scope.startDialogAjaxRequest();

        //var config = {} application/json; charset=utf-8
        var config = {headers: {'Content-Type': 'application/json; charset=UTF-8'}};

//        $scope.addSearchParametersIfNeeded(config, false);
        console.log($scope.contact);
        $http.put(url, $scope.contact, config)
            .success(function (data) {
                $scope.finishAjaxCallOnSuccess(data, "#updateContactsModal", false);
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
            });
    };
    
    $scope.createContact = function (newContactForm) {
        if (!newContactForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }

        $scope.lastAction = 'create';

        var url = $scope.url;

        var config = {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        //var config = {};
//        $scope.addSearchParametersIfNeeded(config, false);

        $scope.startDialogAjaxRequest();

        $http.post(url, $.param($scope.contact), config)
            .success(function (data) {
                $scope.finishAjaxCallOnSuccess(data, "#addContactsModal", false);
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
            });
    };    
    
    $scope.selectedContactAng = function (contact) {
        var selectedContact = angular.copy(contact);
        $scope.contact = selectedContact;
        console.log($scope.contact);
    }    
    
    
    $scope.handleErrorInDialogs = function (status) {
        $("#loadingModal").modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
        if(status == 403 || status == 401){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';
            //window.location.reload();
            window.location = "/trackingNovartis/accessdenied";
            //$location.path('/login');
            //return $q.reject(response);
            
            return;
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    }    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    }    
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        //$scope.tableParams.reload();
        $("#loadingModal").modal('hide');

        if(!isPagination){
            if(modalId){
                $scope.exit(modalId);
            }
        }

        $scope.lastAction = '';
    }    
    
    $scope.exit = function (modalId) {
        console.log("hide "+modalId);
        $(modalId).modal('hide');

        $scope.contact = {};
        $scope.errorOnSubmit = false;
        $scope.errorIllegalAccess = false;
        $scope.displayValidationError = false;
    }
    
    $scope.populateTable = function (data) {
        if (data.pagesCount > 0) {
            $scope.state = 'list';

//            $scope.page = {source: data.contacts, currentPage: $scope.pageToGet, pagesCount: data.pagesCount, totalContacts : data.totalContacts};
            $scope.page = {currentPage: $scope.pageToGet, pagesCount: data.pagesCount, totalContacts : data.totalContacts};

//            if($scope.page.pagesCount <= $scope.page.currentPage){
//                $scope.pageToGet = $scope.page.pagesCount - 1;
//                $scope.page.currentPage = $scope.page.pagesCount - 1;
//            }

            $scope.displayCreateContactButton = true;
            $scope.displaySearchButton = true;
        } else {
            $scope.state = 'noresult';
            $scope.displayCreateContactButton = true;

            if(!$scope.searchFor){
                $scope.displaySearchButton = false;
            }
        }

        if (data.actionMessage || data.searchMessage) {
            $scope.displayMessageToUser = $scope.lastAction != 'search';

            $scope.page.actionMessage = data.actionMessage;
            $scope.page.searchMessage = data.searchMessage;
        } else {
            $scope.displayMessageToUser = false;
        }
    }    
      
    
    $scope.tableParams = new ngTableParams({
        page: 1,
        count: 10
    }, {
        getData: function($defer, params) {
            $scope.startDialogAjaxRequest();
            var config = {params: {page: params.page()}, headers: { 'Content-Type': 'application/json'}};
            console.log(config);
            $http.get($scope.url, config)
                .success(function (data, status) {
                    console.log(data);
                    console.log(data.contacts);
                    console.log(status);
                   $scope.populateTable(data);
                   $scope.displayCreateContactButton = true;
                   params.total(data.totalContacts);
                   $defer.resolve(data.contacts);
                   $scope.finishAjaxCallOnSuccess(data, null, false);
                })
                .error(function (data, status) {
                    $scope.state = 'error';
                    $scope.displayCreateContactButton = false;
                    console.log("error");
                    $scope.handleErrorInDialogs(status);
                });            
        }
    });    
}]);