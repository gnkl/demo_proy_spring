package mx.com.gnkl.novartis.service;

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}
