/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.service.impl;

import mx.com.gnkl.novartis.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import mx.com.gnkl.novartis.repository.jpa.ContactRepository;
import mx.com.gnkl.novartis.bean.ContactListVO;
import mx.com.gnkl.novartis.model.Contact;
import mx.com.gnkl.novartis.repository.ContactDao;
import org.springframework.data.domain.PageImpl;

/**
 *
 * @author jmejia
 */
@Service
@Transactional
public class ContactServiceImpl implements ContactService{
    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ContactDao contactDao;
    
    @Transactional(readOnly = true)
    @Override
    public ContactListVO findAll(int page, int maxResults) {
        Page<Contact> result = executeQueryFindAll(page, maxResults);

//        if(shouldExecuteSameQueryInLastPage(page, result)){
//            int lastPage = result.getTotalPages() - 1;
//            result = executeQueryFindAll(lastPage, maxResults);
//        }

//        Integer totalcontacts = contactDao.getTotalContacts();
//        System.out.println("totalcontacts: "+totalcontacts);
        return buildResult(result);
    }

    @Override
    public Integer getTotalContacts() {
        return contactDao.getTotalContacts();
    }    
    
    @Override
    public void save(Contact contact) {
        contactRepository.save(contact);
    }

    @Secured("ROLE_ADMIN")
    @Override
    public void delete(int contactId) {
        contactRepository.delete(contactId);
    }

    @Transactional(readOnly = true)
    public ContactListVO findByNameLike(int page, int maxResults, String name) {
        Page<Contact> result = executeQueryFindByName(page, maxResults, name);

//        if(shouldExecuteSameQueryInLastPage(page, result)){
//            int lastPage = result.getTotalPages() - 1;
//            result = executeQueryFindByName(lastPage, maxResults, name);
//        }

        return buildResult(result);
    }

    private boolean shouldExecuteSameQueryInLastPage(int page, Page<Contact> result) {
        return isUserAfterOrOnLastPage(page, result) && hasDataInDataBase(result);
    }

    private Page<Contact> executeQueryFindAll(int page, int maxResults) {
        final PageRequest pageRequest = new PageRequest(page, maxResults, sortByNameASC());

        //return contactRepository.findAll(pageRequest);
        Page pagespring = new PageImpl(contactDao.getAllContacts(pageRequest));
        return  pagespring;
    }

    private Sort sortByNameASC() {
        return new Sort(Sort.Direction.ASC, "name");
    }

    private ContactListVO buildResult(Page<Contact> result) {
        return new ContactListVO(result.getTotalPages(), result.getTotalElements(), result.getContent());
    }

    private Page<Contact> executeQueryFindByName(int page, int maxResults, String name) {
        final PageRequest pageRequest = new PageRequest(page, maxResults, sortByNameASC());
        Page pagesprig = new PageImpl(contactDao.findByNameLike(pageRequest, name));
        ///return contactRepository.findByNameLike(pageRequest, "%" + name + "%");
        return pagesprig;
    }

    private boolean isUserAfterOrOnLastPage(int page, Page<Contact> result) {
        return page >= result.getTotalPages() - 1;
    }

    private boolean hasDataInDataBase(Page<Contact> result) {
        return result.getTotalElements() > 0;
    }    
}
