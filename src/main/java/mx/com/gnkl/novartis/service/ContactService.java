/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import mx.com.gnkl.novartis.repository.jpa.ContactRepository;
import mx.com.gnkl.novartis.model.Contact;
import mx.com.gnkl.novartis.bean.ContactListVO;

/**
 *
 * @author jmejia
 */
public interface ContactService {
    
    public ContactListVO findAll(int page, int maxResults);
    public void save(Contact contact);
    public void delete(int contactId);
    public ContactListVO findByNameLike(int page, int maxResults, String name);
    public Integer getTotalContacts();
//    private boolean shouldExecuteSameQueryInLastPage(int page, Page<Contact> result);
//    private Page<Contact> executeQueryFindAll(int page, int maxResults);
//    private Sort sortByNameASC();
//    private ContactListVO buildResult(Page<Contact> result);
//    private Page<Contact> executeQueryFindByName(int page, int maxResults, String name);
//    private boolean isUserAfterOrOnLastPage(int page, Page<Contact> result);
//    private boolean hasDataInDataBase(Page<Contact> result);
}
