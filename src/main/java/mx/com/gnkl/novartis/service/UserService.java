package mx.com.gnkl.novartis.service;

import mx.com.gnkl.novartis.model.User;


public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
