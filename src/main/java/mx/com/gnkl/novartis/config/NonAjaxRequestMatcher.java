/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.config;

import javax.servlet.http.HttpServletRequest;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 *
 * @author jmejia
 */
public class NonAjaxRequestMatcher implements RequestMatcher{
    @Override
    public boolean matches(HttpServletRequest request) {
        return !"XmlHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With"));
    }    
}
