/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.config;


import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 *
 * @author jmejia
 */
public class AjaxAwareAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {
    
    public AjaxAwareAuthenticationEntryPoint(String loginUrl) {
        super(loginUrl);
    }
    
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
            System.out.println(authException.getLocalizedMessage());
            System.out.println(request.getHeaderNames());
            System.out.println(request.getHeader("X-Requested-With"));
            System.out.println(authException);
            //System.out.println(request.
//            while(request.getHeaderNames().hasMoreElements()){
//                String name = (String) request.getHeaderNames().nextElement();
//                System.out.println(name);
//            }
//        //boolean isAjax = request.getRequestURI().startsWith("/protected/");
//        boolean isAjax = request.getRequestURI().contains("/protected/");
          String lookobj = "XMLHttpRequest";
          boolean isAjax = lookobj.equals(request.getHeader("X-Requested-With"));
//        System.out.println("request.getRequestURI(): "+request.getRequestURI());
        if (isAjax) {
            response.sendError(403, "Forbidden");
//            //response.setStatus(403);
//            response.setStatus(401);//endRedirect("login");
        } else {
            super.commence(request, response, authException);
        }        
    }    
}
