/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jdbc;

import java.util.List;
import javax.sql.DataSource;
import mx.com.gnkl.novartis.model.Contact;
import mx.com.gnkl.novartis.repository.ContactDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jmejia
 */
@Repository
public class JdbContactDaoImpl implements ContactDao{

    
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    

    @Override
    public Integer getTotalContacts() {
         return this.jdbcTemplate.queryForObject("select count(*) from Contact", Integer.class);
    }

    @Override
    public List<Contact> getAllContacts() {
        return this.jdbcTemplate.query("select * from Contact", new BeanPropertyRowMapper(Contact.class));
    }

    /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Contact> getAllContacts(PageRequest pagerequest) {
        System.out.println("pagerequest.getPageNumber(): "+pagerequest.getPageNumber());
        System.out.println("pagerequest.getPageSize(): "+pagerequest.getPageSize());
        return this.jdbcTemplate.query("SELECT * FROM Contact LIMIT ?,?", new Object[]{pagerequest.getPageNumber(), pagerequest.getPageSize()},new BeanPropertyRowMapper(Contact.class));
    }

    @Override
    public List<Contact> findByNameLike(PageRequest pagerequest, String name) {
        return this.jdbcTemplate.query("SELECT * FROM Contact WHERE name LIKE ? LIMIT ?,?", new Object[]{"%"+name+"%", pagerequest.getPageNumber(), pagerequest.getPageSize()},new BeanPropertyRowMapper(Contact.class));
    }

    
}
