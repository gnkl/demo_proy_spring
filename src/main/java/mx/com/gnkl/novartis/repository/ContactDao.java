/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository;

import java.util.List;
import mx.com.gnkl.novartis.model.Contact;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author jmejia
 */
public interface ContactDao {
    public Integer getTotalContacts();
    public List<Contact> getAllContacts();
    public List<Contact> getAllContacts(PageRequest pagerequest);
    public List<Contact> findByNameLike(PageRequest pageable, String name);
}
