package mx.com.gnkl.novartis.repository.jpa;

import mx.com.gnkl.novartis.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
